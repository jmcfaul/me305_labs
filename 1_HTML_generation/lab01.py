'''
@file lab01.py
@brief This file contains a script that caculates fibonacci numbers.
@details The user can input a specific index at which to calculate the fibonacci number. The program will output the fibonacci number corresponding to the requested index.
@author Jackson McFaul
@date Ocotober 1, 2020
'''

def fib(n):
    '''
    @Brief    This function calculates fibonacci numbers.
    @detailed This function produces the fibonacci number specified by a given index specified by the user input. Special thanks to Dakota Baker for pointing me towards a method to have the user quit out of the code.
    '''
    print('Welcome to the fibonacci number generator!')
    print('Please enter [end] to quit the program.')
    # Indicates to user the program is running
    while True:
        n = input('Input an index to retrieve its Fibonacci number:')
        if n == 'end':
            break
        else:
            try:
                n = int(n)
                if n == 0:
                    print('Shown below is the fibonacci number at the requested index:')
                    print('-----------------------------------------------------------')
                    # Notifies user that the function is running and the Fibonacci number
                    # is being computed
                    print(0)
                       # If input = 0, function returns 0, this and the following elif 
                       # account for the first 2 indices of the Fibonacci sequence
                elif n == 1:
                    print('Shown below is the fibonacci number at the requested index:')
                    print('-----------------------------------------------------------')
                    print(1)
                elif n > 1:
                    fib_seq = [0,1]
                    x = 2
                    # Introduces a matrix to be filled with the corresponding Fibonacci
                    # numbers up to the index requested. 
                    while x <= n:
                        fib_seq.append(fib_seq[x - 2] + fib_seq[x - 1])
                        x = x + 1
                        # While loop operates with another counter: x. According to the 
                        # index requested, the while loop will fill the matrix with the 
                        # values of the fibonacci sequence up to the specified index.
                        # The function then slices the matrix by the given index to pull
                        # the necessary number out and print it.
                        print('Shown below is the fibonacci number at the requested index:')
                        print('-----------------------------------------------------------')
                    print(fib_seq[n])
                else:
                    print('Please input only positive integers')
            except ValueError:
                    print('This program only works with numbers');
                    
if __name__ == '__main__':
    fib(1)
