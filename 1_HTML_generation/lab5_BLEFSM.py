"""
@file lab5_BLEFSM.py
@brief This file is reponsible for blinking the Nucleo's LED at the specified frequency, as well as reading from, and 
writing to the Nucleo. When the user sends a value between 1 and 10 from the phone, the LED will blink at the given 
input, in Hz. A message confirming this behavior is then sent to the user to read on the phone.
@date 11/4/20
@author: Jackson McFaul
"""
import pyb
import utime
from pyb import UART

# while True:
#     if uart.any() != 0:
#         val = int(uart.readline())
#         if val == 0:
#             print(val,' turns it OFF')
#             pinA5.low()
#         elif val == 1:
#             print(val,' turns it ON')
#             pinA5.high()
#         else:
#             pass
#     else:
#         pass
    
        
class BLEFSM:
    '''
    Parameters
    ----------
    @param S0_init Initialization state, sets FSM interval, frequency timer, UART, and timestamps
    @param S1_LEDblinker Controls turning the LED on and off through pinA5 interaction. Also checks for user input from the phone.
    @param S2_inputcheck If an input is given and is a valid input, S2 takes that input and turns it into a usable integer frequency value.
    ----------
    '''
    S0_init = 0 
    S1_LEDblinker = 1
    S2_inputcalc = 2  
    pinA5 = pyb.Pin(pyb.Pin.cpu.A5)
    
    def __init__(self, interval):
        '''
        Parameters
        ----------
        @param self.interval Sets interval in milliseconds for the FSM to run at
        @param self.frequencytimer Sets period for LED to blink at
        @param self.time_start_1 Sets timestamp for start time
        @param self.time_next_1 Sets timestamp for next run of FSM
        @param self.uart Allows for serial communication between nucleo and smartphone
        @param self.checkpin Internal pin state variable that keeps track of ON vs OFF
        @param self.state Stores FSM state position
        ----------
        '''
        
        self.interval = interval
        self.frequencytimer = 500
        self.period = 1000
        self.time_start_1 = utime.ticks_ms()
        self.time_next_1 = utime.ticks_add(self.time_start_1, int(self.interval))
        #self.time_next_1 = self.time_start_1 + self.interval
        self.uart = UART(3,9600)
        self.checkpin = 0
        self.state = self.S0_init
        
    def transitionto(self, newstate):
        self.state = newstate
        
    def freqchange(self,newfrequency):
        self.period = int((1/newfrequency)*1000)
        self.uart.write('Human, the LED is blinking at '+str(newfrequency)+'  Hz.') 
        
    def runBLE(self):
        self.time_current = utime.ticks_ms()
        if (utime.ticks_diff(self.time_current,self.time_next_1))>0:
            
            if(self.state==self.S0_init):
                self.transitionto(self.S1_LEDblinker)
                
            elif(self.state==self.S1_LEDblinker):
                if(utime.ticks_diff(self.time_current,self.frequencytimer)>=0):
                
                    if(self.checkpin == 0):
                        self.pinA5.high()
                        self.checkpin = 1
                        print('LED ON')
                    elif(self.checkpin ==1):
                        self.pinA5.low()
                        self.checkpin = 0
                        print('LED OFF')
                    
                    self.frequencytimer = utime.ticks_add(self.time_current, int(self.period/2))
                    #self.frequencytimer = utime.ticks_add(self.time_current, 100)        
                    
                if(self.uart.any() != 0):
                    self.transitionto(self.S2_inputcalc)
                else:
                    pass
            
            elif(self.state==self.S2_inputcalc):
                self.userinput = self.uart.read()
                
                # if self.userinput.isdigit() == True and self.userinput >= 1 and self.userinput <=10:
                #     self.userinput = int(self.userinput)
                #     print(str(self.userinput))
                #     self.newfrequency = self.userinput
                #     self.freqchange(self.userinput)
                #     self.transitionto(self.S1_LEDblinker)
                    
                # elif self.userinput.isdigit() ==False:
                #     self.uart.write('Human, thats an invalid input')
                    
                # elif self.userinput == 0:
                #     print('0 Error')
                #     self.uart.write('Human, enter a nonzero Value')
                    
                # elif self.userinput > 10 or self.userinput < 1:
                #     self.uart.write('Human, enter a value between 1 and 10.') 
                
                # else:
                #     pass
                
                self.userinput = int(self.userinput)
                print(str(self.userinput))
                #self.newfrequency = self.userinput
                if(self.userinput>=1 and self.userinput<=10):
                    self.newfrequency = self.userinput
                    self.freqchange(self.userinput)
                    self.transitionto(self.S1_LEDblinker)
                elif(self.userinput <1 or self.userinput >10):
                    self.transitionto(self.S1_LEDblinker)
                    self.uart.write('Human, input a value between 1 and 10')
                
                self.time_next_1 = utime.ticks_add(self.time_next_1, int(self.interval))
                 
        
                 
            
                
                
        
    

