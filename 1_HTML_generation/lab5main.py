"""
@file lab5main.py
@brief 
@date 11/6/20
@author: Jackson McFaul
"""


import pyb
from pyb import UART
import utime


from lab5_BLEFSM import BLEFSM

#uart = UART(3,9600)
# Set interval for FSM to run at 
start_interval = 100 

# Initialize BLEFSM
bluetoothFSM = BLEFSM(start_interval)

# Infinite loop to run FSM
while True:
    bluetoothFSM.runBLE()





 