'''
@file controllertask.py
@brief This file controls the updating of the encoder and setting of duty on the motordriver. Special thanks to Ryan Funchess for helping me 
get this controller working with my other files. My original FSM was not functioning properly, so I switched gears and at least ended up getting data. 
@author Jackson McFaul
@date November 26th 2020
'''

import pyb 
import utime 

class controller:
    '''
    @brief
    '''
    
    def __init__(self, Kp, omega_ref, motor, enc, CL):
        '''
        @brief
        @param
        @param
        '''
        
        self.Kp=Kp
        self.omega_ref = omega_ref
        self.motor = motor
        self.enc = enc
        self.CL = CL
        
    def update(self):
        '''
        @brief
        '''
        self.enc.update()
        RPM = self.enc.get_RPM()
        L = self.CL.omegaupdate(RPM)
        self.motor.set_duty(L)
        return RPM
    