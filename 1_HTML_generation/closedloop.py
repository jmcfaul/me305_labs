'''
@file closedloop.py
@brief This file contains a closed loop controller that allows for gain control with regards to motor voltage input, 
given the current motor speed in RPM.
@author Jackson McFaul
@date November 26th 2020
'''


import pyb 
import utime

class ClosedLoop:
    '''
    @brief this class contains equations necessary for a closed loop controller
    '''

    
    def __init__(self, Kp, omega_ref):
        '''
        @brief
        @param
        @param
        '''
        
        self.Kp = Kp
        self.omega_ref = omega_ref
        
    def omegaupdate(self,omega):
        '''
        @brief omegaupdate calculates the duty input to be sent to the motor driver, and includes cutoff points for max and min limits 
        '''
            
        L = ((self.Kp)/(3.3))*(self.omega_ref-omega)
        if(L>100):
            return 100
            print('100')
        elif(L<-100):
            return -100
            print('-100')
        else:
            return L
            print(str(L))
               
    def get_Kp(self):
        '''
        @brief returns Kp
        '''
        return(self.Kp)
        
    def set_Kp(self, Kp):
        '''
        @brief sets Kp
        '''
        self.Kp = Kp
           
         

        