var classlab6__motordriver_1_1MotorDriver =
[
    [ "__init__", "classlab6__motordriver_1_1MotorDriver.html#a725a1fb09b2cf4b1f0be074c0284eaac", null ],
    [ "disable", "classlab6__motordriver_1_1MotorDriver.html#a7b29a49cc4d52398ce8fb4d0559fab9d", null ],
    [ "enable", "classlab6__motordriver_1_1MotorDriver.html#a3631f6daf6ee726206cff24053047e46", null ],
    [ "set_duty", "classlab6__motordriver_1_1MotorDriver.html#a8e64b6300cfe80a8e909769fd4614e89", null ],
    [ "duty", "classlab6__motordriver_1_1MotorDriver.html#a0989ef2c9b8eac52058ac08c1c0693a0", null ],
    [ "IN1_pin", "classlab6__motordriver_1_1MotorDriver.html#a1cfc8240c1657ea00b25fe339035644b", null ],
    [ "IN2_pin", "classlab6__motordriver_1_1MotorDriver.html#aac442b51d5b3356c553e558fb71a2667", null ],
    [ "nSleep_pin", "classlab6__motordriver_1_1MotorDriver.html#a110bef334798d40b2f174f545b0b348e", null ],
    [ "t3ch1", "classlab6__motordriver_1_1MotorDriver.html#a9693e30e77e56aaa9453695694f48e81", null ],
    [ "t3ch2", "classlab6__motordriver_1_1MotorDriver.html#abd6a0a2b9b9b97da074073d30b6eeaf2", null ],
    [ "timer", "classlab6__motordriver_1_1MotorDriver.html#ad476a62dad1ef9753363845d38543619", null ]
];