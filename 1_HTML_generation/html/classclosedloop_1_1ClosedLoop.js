var classclosedloop_1_1ClosedLoop =
[
    [ "__init__", "classclosedloop_1_1ClosedLoop.html#ae617b5a32ac88bd1f4640fa25aa70ae1", null ],
    [ "get_Kp", "classclosedloop_1_1ClosedLoop.html#aabfd126eb373a40747f7fd312ed0056c", null ],
    [ "omegaupdate", "classclosedloop_1_1ClosedLoop.html#abdd0cbe0c942cc7c0fbc55d47e7bc41a", null ],
    [ "set_Kp", "classclosedloop_1_1ClosedLoop.html#a617a88880b37c7434947936e1d3a37ce", null ],
    [ "Kp", "classclosedloop_1_1ClosedLoop.html#aae8c3c1b5cb36912bca6b14089e108f3", null ],
    [ "omega_ref", "classclosedloop_1_1ClosedLoop.html#ac5e8e5462eb45c6a3095fa163c3e3ce6", null ]
];