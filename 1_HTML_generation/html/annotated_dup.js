var annotated_dup =
[
    [ "closedloop", null, [
      [ "ClosedLoop", "classclosedloop_1_1ClosedLoop.html", "classclosedloop_1_1ClosedLoop" ]
    ] ],
    [ "controllertask", null, [
      [ "controller", "classcontrollertask_1_1controller.html", "classcontrollertask_1_1controller" ]
    ] ],
    [ "controllertask_lab7", null, [
      [ "controller", "classcontrollertask__lab7_1_1controller.html", "classcontrollertask__lab7_1_1controller" ]
    ] ],
    [ "Encoder", null, [
      [ "ENCODER", "classEncoder_1_1ENCODER.html", "classEncoder_1_1ENCODER" ]
    ] ],
    [ "FSM_elevator", null, [
      [ "Button", "classFSM__elevator_1_1Button.html", "classFSM__elevator_1_1Button" ],
      [ "MotorDriver", "classFSM__elevator_1_1MotorDriver.html", "classFSM__elevator_1_1MotorDriver" ],
      [ "TaskElevator", "classFSM__elevator_1_1TaskElevator.html", "classFSM__elevator_1_1TaskElevator" ]
    ] ],
    [ "lab2FSM", null, [
      [ "TaskLEDcontrol", "classlab2FSM_1_1TaskLEDcontrol.html", "classlab2FSM_1_1TaskLEDcontrol" ]
    ] ],
    [ "lab4_Encoder", null, [
      [ "ENCODER", "classlab4__Encoder_1_1ENCODER.html", "classlab4__Encoder_1_1ENCODER" ]
    ] ],
    [ "lab4_encoderdriver", null, [
      [ "runencoder", "classlab4__encoderdriver_1_1runencoder.html", "classlab4__encoderdriver_1_1runencoder" ]
    ] ],
    [ "lab5_BLEFSM", null, [
      [ "BLEFSM", "classlab5__BLEFSM_1_1BLEFSM.html", "classlab5__BLEFSM_1_1BLEFSM" ]
    ] ],
    [ "lab6_encoder", null, [
      [ "encoder", "classlab6__encoder_1_1encoder.html", "classlab6__encoder_1_1encoder" ]
    ] ],
    [ "lab6_motordriver", null, [
      [ "MotorDriver", "classlab6__motordriver_1_1MotorDriver.html", "classlab6__motordriver_1_1MotorDriver" ]
    ] ],
    [ "lab7_motordriver", null, [
      [ "MotorDriver", "classlab7__motordriver_1_1MotorDriver.html", "classlab7__motordriver_1_1MotorDriver" ]
    ] ],
    [ "lab7_motordriver_test", null, [
      [ "MotorDriver", "classlab7__motordriver__test_1_1MotorDriver.html", "classlab7__motordriver__test_1_1MotorDriver" ]
    ] ],
    [ "userinterface", null, [
      [ "TaskUser", "classuserinterface_1_1TaskUser.html", "classuserinterface_1_1TaskUser" ]
    ] ]
];