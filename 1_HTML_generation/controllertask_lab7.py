'''
@file controllertask_lab7.py
@brief This file controls the updating of the encoder and setting of duty on the motordriver. Special thanks to Ryan Funchess for helping me 
get this controller working with my other files. My original FSM was not functioning properly, so I switched gears and at least ended up getting data. 
@author Jackson McFaul
@date November 26th 2020
'''

import pyb 
import utime 

class controller:
    '''
    @brief the controller task includes initialization of necessary objects and operation of both update methods 
    '''
    
    def __init__(self, Kp, omega_ref, motor, enc, CL):
        '''
        @brief creates controller values necessary for update method
        @param Kp from backend main file
        @param omega_ref from backend main file -- this time indexed from a given CSV file for each run 
        @param motor object from backend main file
        @param enc encoder object from backend main file 
        @param CL closed loop object used for calculating L
        '''
        
        self.Kp=Kp
        self.omega_ref = omega_ref
        self.motor = motor
        self.enc = enc
        self.CL = CL
        
    def update_RPM(self):
        '''
        @brief updates encoder, used get_RPM to get the motor speed, sets the duty cycle, and then returns RPM for data generation
        '''
        self.enc.update()
        RPM = self.enc.get_RPM()
        L = self.CL.omegaupdate(RPM)
        self.motor.set_duty(L)
        return RPM
    
    def update_deg(self):
        '''
        @brief added this in for lab 7 in order to supply motor position back to main for J calculation 
        '''
        deg = self.enc.get_deg()
        return deg