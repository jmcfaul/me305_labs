## @file mainpage.py
#  Brief doc for mainpage.py
#
#  Detailed doc for mainpage.py 
#
#  @mainpage
#
#  @section sec_intro Introduction
#  This website hosts the products of my ME-305 labs. An overview of each lab is shown below, with links to source code at the end of each section.
#  Navigation can be accomplished through the use of the menu items on the left, with triangles denoting drop down menus for additional selections.
#
#  @section sec_lab01 Lab01
#  Function for retrieving the Fibonacci number at a selected index.
#  Accomplished through the operation of one function introduced in source code.
#  Each part of the function is described through its construction.
#  Please see https://bitbucket.org/jmcfaul/me305_labs/src/master/lab1/lab01.py for source code.
#
#  @section sec_Lecture Lecture HW1
#  Finite State Machine for operation of an elevator between 2 floors.
#  My code still has a weird attribute error that I can't figure out at the 
#  moment--will update when it is solved.
#  Please see https://bitbucket.org/jmcfaul/me305_labs/src/master/2.%20Lecture%20Homework/ for source code.
#  FSM diagram can be seen here: https://bitbucket.org/jmcfaul/me305_labs/src/master/2_Lecture%20Homework/McFaul_FSMdiagram.png 
#
#  @section sec_lab02 Lab02
#  A finite state machine with multitasking that controls a virtual LED, as well as a physical LED on 
#  my nucleo board. Please see https://bitbucket.org/jmcfaul/me305_labs/src/master/lab2/ for source code.
#
#  @section sec_lab03 Lab03
#  Lab 3 entails a multitude of files used to interact with my nucleo board and an encoder on a motor. When ran
#  this code keeps track of the encoder's position, and allows the user to either reset that position, read that 
#  position, or read the change between the encoder's current position and last position. 
#  Please see https://bitbucket.org/jmcfaul/me305_labs/src/master/lab3/ for source code.
#  
#  @section sec_lab04 Lab04
#  Lab 4 builds on the code created in lab 3--this time with a user interface module that is run in python. This code 
#  interacts with a main file on the nucleo board that automatically initializes when the board is plugged in. The user
#  can input a command to begin data collection, as well as a command to terminate data collection. Once data collection is 
#  stopped, the data is saved in a CSV file, and plotted for visual reference.
#
#  Note: My code still doesn't fully work yet--I am still having an empty list error, as there is some problem either taking data, or 
#  sending it from the nucleo board. I will be working to figure this out. End of quarter update: This code is still very broken and I
#  have not had the time to come back and fix it. This quarter has certainly been overwhelming, but I'm hoping my completion of the rest 
#  of the labs will help make up for this one.
#
#  Please see https://bitbucket.org/jmcfaul/me305_labs/src/master/lab4/ for source code.
#
#  Please see https://bitbucket.org/jmcfaul/me305_labs/src/master/lab4/McFaul_lab4FSM.JPG for an image of my FSM
#
#  @section sec_lab05 Lab05
#  Lab 5 transitions from the previous labs and begins focussing on interaction between the nucleo and our smartphones. I constructed
#  this code as a FSM with the bluetooth communication driver integrated into the FSM. I realized later that what was really desired was
#  a bluetooth driver independent of the FSM, so I will edit this code hopefully before the end of the quarter to reflect those changes.
#  Additionally, it is worth noting the construction of my app and code with regards to rejecting letters. I was not able to get my code
#  to function properly while rejecting characters, so as a workaround I adjusted the input keyboard on the app to be the numberic 
#  keyboard only. It is not a perfect solution, but its something. I'll try to integrate non-number character rejection eventually. 
#
#  Please see https://bitbucket.org/jmcfaul/me305_labs/src/master/lab5/ for python source code.
#
#  Please see https://x.thunkable.com/copy/dc650cf48176fe944f81a45e29b04d81 for IOS source code. [Link expires 1-12-2021]
#
#  Please see https://bitbucket.org/jmcfaul/me305_labs/src/master/lab5/McFaul-lab6FSM.jpeg for my FSM and task sketches.
#
#  @section sec_lab06-1 Lab06 Part 1  
#  Lab 6 part 1 includes a motordriver file that controls a DC motor using pulse width modulation. The motor can be told to spin 
#  in forward or in reverse depending on the sign of the input value. 
#
#  Please see https://bitbucket.org/jmcfaul/me305_labs/src/master/lab6/lab6_motordriver.py for source code.
#
#  @section sec_lab06-2 Lab06 Part 2
#  Lab 6 Part 2 entails creating the rest of the backend that will direct interactions between the computer, nucleo, and multiple drivers.
#  After a ton of troubleshooting and setbacks, it was decided that I possibly had some faulty hardware with regards to my motors. Once I 
#  swapped the kit out for a new one, my motordriver began working successfully. Unfortunately this still was a fairly large setback especially 
#  for debugging time, and with the rest of life going on I was at least able to make something work here. I could not get my UI to work back and 
#  forth with my backend, but I did however get my encoder to read data that I could see on the nucleo. By using an online comma-separated list converter
#  link here: https://convert.town/comma-separated-list-to-column I was able to put this data into excel and observe how changing my Kp affected 
#  the motor control. 
#
#  Please see https://bitbucket.org/jmcfaul/me305_labs/src/master/lab6/ for source code 
#
#  Please see https://bitbucket.org/jmcfaul/me305_labs/src/master/lab6/McFaul-lab6_TD.jpeg for my task diagram sketch 
#
#  Please see https://bitbucket.org/jmcfaul/me305_labs/src/master/lab6/lab6_data.xlsx for a download of my excel data file 
#
#  Please see https://bitbucket.org/jmcfaul/me305_labs/src/master/lab6/Kp%20Tuning%20Graphs/ for a folder containing my Kp tuning plots
#  or navigate to the Kp Tuning Plots page on the left for further discussion of each graph.
#
#  @section sec_lab07 Lab07
#  Lab 7 is the final part to ME305, where we take our set point control and modify it to track a reference profile given by a CSV file. The CSV file 
#  contains time, RPM, and position (in degrees) data for every millisecond. As mentioned above in lab06 part 2, my code does not function with my UI--
#  I kept having trouble passing floats back and forth, and instead of pursue that rabbit hole, I decided to switch gears and get data so that I could 
#  provide some meaningful discussion with regards to my Kp tuning. Because I had to run everything on the nucelo, I was severely limited by RAM. I will further 
#  discuss how I solved this and created my plots on the Lab 07 Response Curves page, which can be found in the menu items on the left. I specifically discuss
#  how I edited the CSV file in the methodology section of the Response Curves page.  
# 
#  Please see https://bitbucket.org/jmcfaul/me305_labs/src/master/lab7/ for source code 
#
#  Please see https://bitbucket.org/jmcfaul/me305_labs/src/master/lab6/lab6_and_7_data.xlsx for a download of my excel data file
#
#  Please see https://bitbucket.org/jmcfaul/me305_labs/src/master/lab7/Response%20Plots/ for a folder containing my response plots
#  or navigate to the Lab 07 Response Curves page, located in the menu items on the left
#  
#  @author Jackson McFaul
#
#  @copyright Cal Poly Mechanical Engineering
#
#  @date Updated December 4th, 2020
#
#  @page page_Lab06-Part2 Lab 06 Kp Tuning Plots 
# 
#  @section sec_intro Introduction 
#  The plots shown below demonstrate the tuning of my controller through iteration of various Kp values. A main takeaway from these plots that I noticed very early on is that 
#  the motor assembly has a ton of friction and loss from the input shaft to the output shaft. This can be seen due to the fact that my setpoint reference was 1600 RPM,
#  but when the motor reached steady state for my more effective Kp values, the actual motor speed was around 1000 RPM. This will certainly be problematic for lab 7. 
#  
#  @section sec_method Methodology
#  Because I decided to move past trying to get my UI to work, I had to create these plots using the nucleo, putty, and online list converter, and excel. Once I got the code running,
#  I input a Kp value to test, with 1600 RPM being the setpoint for every run. I then executed my main backend file. Time and motor speed data were output into comma separated lists,
#  which I then inserted into a list converter that can be found here: https://convert.town/comma-separated-list-to-column  . This took what would originally be one cell of 100s of numbers
#  in excel, and converted them into a usable column of data. I then pasted this data into an excel workbook and created plots from there. Rinse, and repeat for various Kp values--not ideal, 
#  but hopefully worth something. The plots are discussed below.
#
#  @section sec_plots Motor Speed vs Time plots 
#  Kp tuning plots can be seen below, section tites with Kp values are located above the figures, and figure captions are located below the figures:
#
#  @section sec_plot1 Kp = .25 
#  @image html Gain_25.png
#  Figure 1: Kp = .25-- I started here because I had done a lot of messing around with larger values of Kp already, and even though I hadn't seen numbers yet, it was obvious the 
#  motor hopped around. This ended up being my best run compared to the rest of the values I tested, as can be seen by the motor speed reaching a fairly steady state value after 
#  a quick spike to around 1500 RPM. As discussed previously, I should really be scaling up the voltage even more in order to close the gap between the target RPM and the reference value.
#  Scaling up the voltage would account for losses that aren't included in any of the duty level calculations. 
#
#  @section sec_plot2 Kp = .50 
#  @image html Gain_50.png
#  Figure 2: Kp = .50-- doubled the gain value here to see if a higher gain would force the motor to steady state more quickly. We can see here that the opposite is true, and the 
#  controller is now overcompensating. I believe this is party due to the larger gain value but also because there is friction in the system. Once the motor pushes past that initial ramp of friction
#  it is much easier for the motor to spin. With this assumption, it makes sense that the RPM would jump back and forth more aggresively with higher gain. Honestly, the plots only get worse from here
#  but I included them anyways.
# 
#  @section sec_plot3 Kp = .75
#  @image html Gain_75.png
#  Figure 3: Kp = .75-- We can see here that the controller is having trouble overcompensating due to the larger gain, and is not able to reach steady state. 
# 
#  @section sec_plot4 Kp = 1.00
#  @image html Gain_100.png
#  Figure 4: Kp = 1.00-- Included for kicks, it is interesting how there is so much variability in the results--I noticed some more inconsistencies after this one.
# 
#  @section sec_plot5 Kp = 3.00
#  @image html Gain_300.png
#  FIgure 5: Kp = 3.00-- The interesting part about this graph is that the amplitude of the spikes appears to be less than with a smaller gain value. We still didn't reach steady state, but I 
#  hypothesize that as the motor is repeatedly run, the friction in the system changes somehow. 
# 
#  @section sec_plot6 Kp = 5.00
#  @image html Gain_500.png
#  Figure 6: Kp = 5.00-- Included for your viewing pleasure.
# 
#  @section sec_plot7 Kp = 10.00
#  @image html Gain_1000.png
#  Figure 7: Kp = 10.00-- Included for your viewing pleasure. 
# 
#  @page page_Lab07plots Lab 07 Response Curves 
# 
#  @section sec_intro2 Introduction 
#  The plots shown below demonstrate the tuning of my controller, but this time through reference value control instead of a set RPM value. As mentioned previously, I did not use UI control 
#  on the computer, but instead did everything on the nucleo. The methodology section below describes how I accomplished this, and specifies how I modified the reference profile. I have also
#  provided a screenshot of what my putty output looks like in order to prove that I am calculating J with the code and not in my excel file. The Reference and Response Plots section contains 
#  four sets of reponse values, each with the reference plots shown directly above them for convenience during comparison. The plots are individually discussed as well. Please note: each figure 
#  caption begins with its respective Kp and J values, All of this data, including the recorded J values can be viewed in my excel workbook located here: 
#  https://bitbucket.org/jmcfaul/me305_labs/src/master/lab6/lab6_and_7_data.xlsx 
#  
#  @section sec_method2 Methodology
#  Building off the methodology from lab 6 part 2--I adapted my code to track the given reference profile in the CSV file. All of the problems I worked around came back to what seemed to be the 
#  available RAM on the nucleo, as well as the fact that my code is bulky, and does not utilize the PC. First, I filtered the CSV file for data every 35 ms, which was the interval I used for 
#  lab 6. I then removed the time column to conserve file size, and loaded it onto the nucleo. I have seen on piazza that other students had the same problem I did, which I've mentioned 
#  a couple times now--motor friction. To combat this, I scaled up the reference RPM by 75, and adjusted the position data accordingly based off those new RPM values, and my chosen time 
#  interval. An important note: each time I edited the RPM and position values, I made sure to plot everything in excel to ensure I was getting the same shape as the original graphs.
#  Scaling everything up ended up increasing the file size as well, because now instead of 30 RPM we're at 2250, and insetad of 720 deg, we're in the 20,000's. With data every 35ms, one can 
#  guess that there were considerably more digits. I am not quite sure if this is the root cause to the issues I had, but I continued to edit the reference file and my task interval until 
#  stuff worked. Instead of every 35ms, I filtered for data every 100ms. The issue here is that my code wouldn't even make it out of reading the CSV before the RAM was gone, much less create 15
#  seconds worth of response data. So, now I have RPM and position data every 100ms, and I'm running my interval at 100ms, which again, is slow and it goes back to how I set up my code, which I acknowledge, but 
#  just hang with me because I needed to do one more thing to get it to work. At this point, I was running out of RAM in the middle of generating my response lists, so I changed the sampling in my 
#  code to every 200ms, and bam it all worked. At this point I was more concerned with producing plots and seeing if I could get my J value to change at all, so that is what I did from there. 
#  An example of my putty output is shown below, which includes the calculation of J, which I will discuss in the plot captions.
#
#  @image html putty_example.png
#  Shown above is my putty output, with time, RPM response, and position response data. I used the same strategy as lab6 part 2, with converting these lists into usable columns of data, 
#  and then creating my plots in excel. 
# 
#  @section sec_plots2 Reference and Response Plots  
#  Motor speed and motor position reference and response plots can be seen below, section titles come above the figures, and figure captions are located below the figures:
#
#  @section sec_resp1 [Kp = 0.1] [J = 1.19E9]
#  @image html kp_1.png
#  Figure 1: [Kp = 0.1] [J = 1.19E9]
#  Reference RPM and reference position are included as the top two plots of each figure. I started with smaller gain values based off the performance data from lab 6 part 2
#  and we can see here that I at least got some sort of response that followed the reference profile, which I was happy with. The main thing here is that because my task interval
#  is so slow, I think that could be affecting the RPM data I'm getting. It is probably a combination of that and motor friction, where the data is sampled right at the point 
#  where the adjustment is ocurring, or there is still something wrong with my encoder deltas. As for position, we see a much nicer plot that seems to very smoothly follow the 
#  reference after 5 seconds. It is pretty easy to see why my J is so large: 1. Because of those RPM data points under 5 seconds, and 2. Because motor friction severely limits
#  the actual position the motor shaft reaches. While the curve is smooth, we can see that it is offset by around 35,000 degrees! This definitely contributes to the insane error 
#  I'm calculating, but nonetheless at least everything is somewhat working. 
#
#  @section sec_resp2 [Kp = 0.20] [J = 1.01E9] 
#  @image html kp_2.png
#  Figure 2: [Kp = 0.20] [J = 1.01E9]
#  This was the best run I got in terms of J values. The motor data seems to be more choppy, however the larger gain improved the motor response after 5 seconds. The main 
#  observation here with regards to J calculation is that the positional data is far less offset compared to Kp = 0.10. This contributed to far less error on that end, resulting
#  in a considerably smaller J value. 
# 
#  @section sec_resp3 [Kp = 0.30] [J = 1.12E9]
#  @image html kp_3.png
#  Figure 3: [Kp = 0.30] [J = 1.12E9]
#  I tried to see if more gain would further improve the positional data, but things started to get pretty weird here. I mentioned this once previously, but repeatedly running the motor
#  I believe gives rise to different frictional characteristics that skew the data even more. The most consistency I saw was actually when I let the motors sit overnight, and then tried
#  running code the next day. Similar values of Kp would give different outputs of J when comparing runs after previous motor use to runs after the motor had been sitting for a while. 
#  Nonetheless, the response was crazy, but still gave less error than Kp = 0.1, which I still think is due to the difference in positional data.
# 
#  @section sec_resp4 [Kp = 0.25] [J = 1.08E9]
#  @image html kp_25.png
#  Figure 4: [Kp = 0.25] [J = 1.08E9]
#  I tried toning it down again, and I conducted this run after letting the motor rest for a couple minutes. Still not as good as Kp = .20, but the takeaway here is that .25 is in between 
#  .20 and .30, and I got a J value that followed suit. Therefore, I concluded my testing here. 
# 












































 