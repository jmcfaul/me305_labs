# -*- coding: utf-8 -*-
"""
@file lab4_main.py
@brief main.py operates on the nucleo board once the board is plugged into the COM port. 
Once the user gives an input of G or g, the nucleo begins collecting data.
@author Jackson McFaul
@date Ocotober 25, 2020
'''
"""

import pyb
from Encoder import ENCODER
from lab4_encoderdriver import runencoder
from pyb import UART


#Input timer 3 as before in lab 3, set period and prescaler values 
tim = pyb.Timer(3)
tim.init(prescaler = 0, period = 0xFFFF)
tim.channel(1, pin=pyb.Pin.cpu.A6, mode=pyb.Timer.ENC_AB)
tim.channel(2, pin=pyb.Pin.cpu.A7, mode=pyb.Timer.ENC_AB)

#encoder receives time and period       
encoder = ENCODER(tim)
uart_1 = UART(2)
time_ms = 100

#looks for user input after receiving a "1" and the proceeds out of the loop to 
#run task1. 
n = 0 
while(n ==0):
    if uart_1.any() != 0:
        eggandcheeseomelet = uart_1.readchar()
        if eggandcheeseomelet == 71:
            n = 1

#runencoder controls timing of encoder updates 
task1 = runencoder(encoder, time_ms, uart_1)
            
while(True):
    task1.run()
    
    
        