'''
@file lab6_encoder.py
@brief This file stores the encoder class. This class is controlled by runencoder.py. When encoder updates, 
its position is calculated based on delta. Because overflow and underflow exist, correction protocol has 
been included. Delta is corrected when its magnitude is >= 1/2*period. 
@author Jackson McFaul
@date Ocotober 20, 2020
'''



class encoder:
    '''
    @brief      The encoder class encapsulates the operation of a timer.
    @details    This class will set up all the parameters necessary for this lab, as well as house 
    numerous methods required for the operation of our counter. 
    '''
    position = 0 
    reset = 0
    def __init__(self, tim, period, interval):
        '''
        @brief creates the encoder object
        
        Parameters
        ----------
       @param tim Creates a timer object connected to each channel.
       @param period Simply restates period for use in calculations here.
       @param interval 
        ----------
        '''
        
        self.tim = tim
        self.period = period
        self.interval = interval
        
    def update(self):
        
        counter_current = self.tim.counter()
        delta = self.get_delta(self.position + self.reset, counter_current)
        
        if(abs(delta)>=(self.period/2)):
            if(delta>0):
                delta = delta - self.period
            else:
                delta = delta + self.period
                            
        self.delta = delta
        new_position = self.position + delta
        self.set_position(new_position)
        
    def get_position(self):
        '''
        @brief Gets encoder position
        '''
        return self.position
        
        
    def set_position(self, new_position):
        '''
        @brief Sets encoder position
        '''
        self.position = new_position
        
        
    def get_delta(self, position, counter_current):
        '''
        @brief Returns a difference of 2 values
        '''
        
        delta = self.tim.counter() - position
        self.delta=delta
        return delta
    
    def get_RPM(self):
        revs = (self.delta)/4000
        dtime= self.interval
        RPM = revs/(dtime/(60000))
        return RPM
        
    # def transitionTo(self, newState):
    #     '''
    #     @brief Updates the state variable
    #     '''
    #     self.state = newState
        
# if __name__ == '__main__':
#     taskinterval = 25
#     tim = pyb.Timer(4)
#     #enctim4.init(prescalar = 0, period = 0xFFFF)
#     tim.channel(1,pin=pyb.Pin.cpu.B6,mode=pyb.Timer.ENC_AB)
#     tim.channel(2,pin=pyb.Pin.cpu.B7,mode=pyb.Timer.ENC_AB)

#     enc = encoder(tim, 0xFFFF, taskinterval)     
    