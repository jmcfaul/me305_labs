# -*- coding: utf-8 -*-
'''
@file userinterface.py
@brief This is the user interface file. Here, UART is utilized to wait for keyboard input from the user.
This files is connected to the shared_variables.py in order to update user commands. 
@author Jackson McFaul
@date Ocotober 20, 2020
'''

import pyb 
import utime
import shared_variables

class TaskUser:
    
    def __init__(self, elapse_2):
        '''
        @brief creates the TaskUser object
        
        Parameters
        ----------
       @param ser Uses UART to look at keyboard inputs
       @param time_start_2 Timestamp for start time
       @param elapse_2 Timestamp for elapsed time between runs
       @param time_adjust_2 Timestamp for iterating the next run
        ----------
        '''
        self.ser=pyb.UART(2)
        
        self.time_start_2 = utime.ticks_ms()
        self.elapse_2 = elapse_2
        self.time_adjust_2 = utime.ticks_add(self.time_start_2, self.elapse_2)
        
        print('------------------------------------------------------------')
        print('Hello human, please type one of the following commands:')
        print('To reset the encoder position, type: Z')
        print('To print the encoder position, type: P')
        print('To print the encoder delta,    type: D')
        print('------------------------------------------------------------')
        
    def run(self):
        self.time_current_2 = utime.ticks_ms()
        if utime.ticks_diff(self.time_current_2, self.time_adjust_2)>0:
            if(self.ser.any()>1):
                    print('Silly human, please type only ---1--- command.')
            elif(self.ser.any()==1):
                userinput = self.ser.readchar()
                if(userinput==80 or userinput==112): #ASCII Values found from w3resource.com
                    print(str(shared_variables.position))
                elif(userinput==100 or userinput==68):
                    print(str(shared_variables.delta))
                elif(userinput==122 or userinput==90):
                    shared_variables.cmd = 1
                else:
                    print('Silly human, try again')