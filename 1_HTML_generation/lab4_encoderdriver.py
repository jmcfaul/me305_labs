# -*- coding: utf-8 -*-
'''
@file lab4_encoderdriver.py
@brief This file holds an FSM which updates the encoder while checking for user input from the UI frontend. While the encoder is updated,
data is stored in a list on the board that is sent over the serial port, where it is then stripped and formatted into CSV format for 
plotting and file saving.
@author Jackson McFaul
@date Ocotober 25, 2020
'''

import utime
class runencoder:
    '''
    Parameters
    ----------
    @param S0_init State 0: intializes everything
    @param S1_getdata State 1: gets data from last lab's encoder updates'
    @param S2_checkinput State 2: checks for user input to terminate data collection
    @param S3_format State 3: formats time and position lists and sends to computer
    ----------
    '''
    S0_init =       1
    S1_data =       2
    S2_checkinput = 3
    S3_format =     4
    
    def __init__(self, encoder, elapse_1, uart_1):
        '''
        @brief creates the runencoder object
        
        Parameters
        ----------
       @param encoder Makes an encoder object to be updated
       @param elapse_1 Elapsed time between runs
       @param time_start_1 Timestamp for start time
       @param time_adjust_1 Timsetamp for next run
        ----------
        '''
        #unchanged from lab 3 
        self.encoder = encoder
        self.elapse_1 = elapse_1
        self.time_start_1 = utime.ticks_ms()
        self.time_adjust_1 = utime.ticks_add(self.time_start_1, self.elapse_1)
        self.state = self.S0_init
        
        #Added uart communication with main file 
        self.uart_1 = uart_1
        
    def run(self):
        self.time_current = utime.ticks_ms()
        if utime.ticks_diff(self.time_current, self.time_adjust_1)>0:
            
            #state 0: initialization and empty list generation 
            if(self.state==self.S0_init):
                self.position_list = []
                self.timelist = []
                self.transitionto(self.S1_data)
            
            #state 1: updates encoder and populates lists with time and position values 
            elif(self.state ==self.S1_getdata):
                self.encoder.update()
                self.timelist.append(self.time_current - self.time_start_1)
                self.position_list.append(self.encoder.get_position())
                self.time_adjust_1 = utime.ticks_add(self.time_adjust_1, self.elapse_1)
                self.transitionto(self.S2_checkinput)
            
            #state 2: check for termination from user, or terminate after 10 seconds
            elif(self.state==self.S2_checkinput):
                if self.uart_1.any() != 0:
                    eggandcheeseomelet = self.uart_1.readchar()
                else: 
                    eggandcheeseomelet = 0
            
                if (eggandcheeseomelet ==83 or eggandcheeseomelet==115 or self.time_current - self.time_start_1 >10000):
                    self.transitionto(self.S3_format)
                else:
                    self.transitionto(self.S1_data)
            
            #state 3: format lists to be sent over serial port separated by commas
            #I am currently running into an empty list issue
            #notify completion of FSM
            
            elif(self.state ==self.S3_format):
                self.uart_1.write('{:}\r\n'.format(len(self.position_list)))
                for n in range(len(self.position_list)):
                    self.uart.write('{:},{:}\r\n'.format(self.timelist[n], self.position_list[n]))
                     
                print('COMPLETE')
    
            self.time_adjust_1 = utime.ticks_add(self.time_start_1, self.elapse_1)
        
        
    def transitionto(self, newstate):
        '''
        @brief Updates the state variable
        '''
        self.state=newstate
