'''
@file lab6_main.py
@brief This file runs on the nucleo and is responsible for controlling the interaction of all my files. Because I wasn't able to get the UI to work,
I instead just plotted data here that I could view in putty. Then, I used https://convert.town/comma-separated-list-to-column to translate that list
into something I could view in excel to evaluate my Kp tuning. The graphs can be viewed in my repository.'
@author Jackson McFaul
@date November 26th 2020
'''


import pyb 
from pyb import UART
import utime
from controllertask import controller
from closedloop import ClosedLoop
from lab6_encoder import encoder
from lab6_motordriver import MotorDriver

#communication with board 
uart1 = pyb.UART(2)


#test values 
Kp = .25
omega_ref = 1600
taskinterval = 35

tim = pyb.Timer(4)
tim.init(prescaler = 0, period = 0xFFFF)
tim.channel(1,pin=pyb.Pin.cpu.B6,mode=pyb.Timer.ENC_AB)
tim.channel(2,pin=pyb.Pin.cpu.B7,mode=pyb.Timer.ENC_AB)

pinA15 = pyb.Pin(pyb.Pin.cpu.A15,pyb.Pin.OUT_PP)
pinB4 = pyb.Pin(pyb.Pin.cpu.B4)
pinB5 = pyb.Pin (pyb.Pin.cpu.B5)
tim3 = pyb.Timer(3, freq = 20000)        

enc = encoder(tim, 0xFFFF, taskinterval)        
motor = MotorDriver(pinA15, pinB4, pinB5, tim3)
motor.enable()

CL = ClosedLoop(Kp, omega_ref)
control = controller(Kp, omega_ref, motor, enc, CL)


timelist = []
speedlist = []

start_time = utime.ticks_ms()
next_time = utime.ticks_add(start_time, taskinterval)


#build data lists
#wait for 1.5 seconds to acquire data
while((next_time-start_time)<1500):
    time_current = utime.ticks_ms()
    if utime.ticks_diff(time_current, next_time)>0:
        motorspeed = control.update()
        time_delta = time_current - start_time
        timelist.append(time_delta)
        speedlist.append(motorspeed)
        next_time = utime.ticks_add(next_time, taskinterval)

        
#printing lists here so that I can input into excel and observe my tuning 
print(timelist)
print('----------------------------------------------------------')
print(speedlist)
print('----------------------------------------------------------')

motor.disable()      
        
