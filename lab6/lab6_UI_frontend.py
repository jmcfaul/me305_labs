"""
@file lab6_UI_frontend.py
@brief This UI is run on the computer and sends a Kp and reference RPM to the backend on the nucleo. This file is currently unfinished because
of debugging issues on the backend side. In an ideal world, this file would receive values and produce plots, but I have done that manually usuing 
putty and excel. 
@author Jackson McFaul
@date November 26, 2020
'''
"""

import time 
import serial
import numpy 
import matplotlib 

#start serial port communication with board 
ser = serial.Serial(port='COM4', baudrate=115273, timeout = 1)

print('----------------------------------------------------------')
print('Motor Control System UI Front End')
print('----------------------------------------------------------')
Kp = input('Type a value for Kp ')
Kp = float(Kp)

print('----------------------------------------------------------')
omega_ref = input('Type an RPM value for omega reference ')
omega_ref = float(omega_ref)

print('----------------------------------------------------------')


#plot and label
#matplotlib.pyplot.plot(, )
#matplotlib.pyplot.xlabel('Time [ms]')
#matplotlib.pyplot.ylabel('Velocity [RPM]')    



