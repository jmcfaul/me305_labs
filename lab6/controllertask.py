'''
@file controllertask.py
@brief This file controls the updating of the encoder and setting of duty on the motordriver. Special thanks to Ryan Funchess for helping me 
get this controller working with my other files. My original FSM was not functioning properly, so I switched gears and at least ended up getting data. 
@author Jackson McFaul
@date November 26th 2020
'''

import pyb 
import utime 

class controller:
    '''
    @brief the controller task includes initialization of necessary objects and operation of update method 
    '''
    
    def __init__(self, Kp, omega_ref, motor, enc, CL):
        '''
        @brief creates controller values necessary for update method
        @param Kp from backend main file
        @param omega_ref from backend main file
        @param motor object from backend main file
        @param enc encoder object from backend main file 
        @param CL closed loop object used for calculating L
        '''
        
        self.Kp=Kp
        self.omega_ref = omega_ref
        self.motor = motor
        self.enc = enc
        self.CL = CL
        
    def update(self):
        '''
        @brief updates encoder, calculates motor RPM, calculates duty level from given equation discussed in lab, and sets motor duty cycle using this L value
        returns RPM for list generation
        '''
        self.enc.update()
        RPM = self.enc.get_RPM()
        L = self.CL.omegaupdate(RPM)
        self.motor.set_duty(L)
        return RPM
    