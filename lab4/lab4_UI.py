# -*- coding: utf-8 -*-
"""
@file lab4_UI.py
@brief The UI front end runs from the computer in pyton. The user can begin data collection by inputting "G" or "g" 
when prompted. When this happens, the nucleo begins collecting data from the encoder. If the user inputs "S" or "s", 
or if 10 seconds pass, the data collection stops. After this, time and position data are sent back to the computer and plotted, 
as well as saved in a CSV file.
Code is unfinished due to backend issues.

@author Jackson McFaul
@date Ocotober 25, 2020
'''
"""
import serial
import time 
import matplotlib 

#start serial port communication with board 
ser = serial.Serial(port='COM4', baudrate=115273, timeout = 1)
print('----------------------------------------------------------')
print('Hello again silly human, are you ready to collect some data?')
print('----------------------------------------------------------')
print('To begin data collection, type:  G  ')
print('To end data collection, type:    S  ')
print('----------------------------------------------------------')

#take input from user, send signal to board to begin data collection 
#n = 0
#test values:
n = 1
     
ser.write('G'.encode('ascii'))

#Check for termination   
#insert stop check here

#empty list generation and time delay so that board receives all inputs        
time_list =[]
position_list =[]
time.sleep(5)

#read, trip, and split time and position data for plottiung and CSV output
#for n #insert control loop variable here
    dataline = ser.readline().decode('ascii')
    dataline = (dataline.strip())
    datalist = dataline.split(',')
    time_list.append(int(datalist[0]))
    position_list.append(int(datalist[1]))

#plot and label
matplotlib.pyplot.plot(time_list,position_list)
matplotlib.pyplot.xlabel('Time [ms]')
matplotlib.pyplot.ylabel('Steps [units]')    

