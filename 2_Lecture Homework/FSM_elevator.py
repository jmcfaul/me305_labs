'''
@file FSM_elevator.py
@author Jackson McFaul
@date Ocotober 7, 2020

This file is my attempt at Homework 1 - where I was tasked with modifying an 
example FSM to fit the requirements of an elevator operating between two floors.

There is a button on floor 1 and a button on floor 2.

Additionally, there are sensors that detect which floor the elevator is at. 
'''

from random import choice
import time

class TaskElevator:
    '''
    @brief      A finite state machine to control an elevator.
    @details    This class implements a finite state machine to control the
                operation of and elevator traveling between 2 floors.
    '''
    
    ## Constant defining State 0 - Initialization
    S0_INIT                  = 0    
    
    ## Constant defining State 1
    S1_MOVING_DOWN           = 1    
    
    ## Constant defining State 2
    S2_MOVING_UP             = 2    
    
    ## Constant defining State 3
    S3_STOPPED_AT_BOTTOM     = 3    
    
    ## Constant defining State 4
    S4_STOPPED_AT_TOP        = 4
    
    ## Constant defining State 5
    S5_DO_NOTHING            = 5
    
    def __init__(self, interval, Button_1, Button_2, first, second, Motor, sens_2):
        '''
        @brief            Creates a TaskElevator object.
        @param Button_1     An object from class Button representing 1st floor button 
        @param Button_2     An object from class Button representing 2nd floor button
        @param floor_1      An object from class Button representing floor 1 
        @param floor_2      An object from class Button representing floor 2
        @param Motor        An object from class MotorDriver representing an elevator motor.
        @param first        An object from class Button that represents the first floor sensor
        @param second       An object from class Button that represents the second floor sensor
        '''
        
        ## The state to run on the next iteration of the task.
        self.state = self.S0_INIT
        
        ## Sets Button 1 to 0 as requested.
        self.Button_1 = Button_1
        self.Button_1.setButtonState(0)
        
        ## Sets Button 2 to 0 as requested. 
        self.Button_2 = Button_2
        self.Button_2.setButtonState(0)
        
        ## The sensor that detects when the elevator is at floor 1
        self.first = first
        
        ## The sensor that detects when the elevator is at floor 2
        self.second = second
        
        ## The motor object that controls elevator motion
        self.Motor = Motor
        
        ## Counter that describes the number of times the task has run
        self.runs = 0
        
        ##  The amount of time in seconds between runs of the task
        self.interval = interval
        
        ## The timestamp for the first iteration
        self.start_time = time.time()
        
        ## The "timestamp" for when the task should run next
        self.next_time = self.start_time + self.interval
        
        ## Set Buttons to 0 as requested in homework assignment
    
        
    def run(self):
        '''
        @brief      Runs one iteration of the task
        '''
        self.curr_time = time.time()
        if self.curr_time > self.next_time:
            
            if(self.state == self.S0_INIT): # Moves elevator down at the start.
                self.transitionTo(self.S1_MOVING_DOWN)
                self.Motor.GoDOWN
                
                print(str(self.runs) + ': State 0 ' + str(self.curr_time-self.start_time))
            
            elif(self.state == self.S1_MOVING_DOWN):
                if(self.first.getsensorstate()==1): # have sensor check if elevator is on first floor
                    self.transitionTo(self.S3_STOPPED_AT_BOTTOM)
                    self.Motor.STOP()
                    self.Button1.setButtonState(0)
                
                print(str(self.runs) + ': State 1 ' + str(self.curr_time-self.start_time))
            
            elif(self.state == self.S2_MOVING_UP):
                if(self.second.getsensorstate()==1): # have sensor check if elevator is on second floor
                    self.transitionTo(self.S4_STOPPED_AT_TOP)
                    self.Motor.STOP()
                    self.Button2.setButtonState(0)
                
                print(str(self.runs) + ': State 2 ' + str(self.curr_time-self.start_time))
            
            elif(self.state == self.S3_STOPPED_AT_BOTTOM):
                if(self.Button_1.getButtonState()==1): #checks if 1st floor button is pressed, then clears it
                    self.setButtonState(0)
                if(self.Button_2.getButtonState()==1):
                    self.transitionTo(self.S2_MOVING_UP)
                    self.Motor.GoUP
                    
                print(str(self.runs) + ': State 3 ' + str(self.curr_time-self.start_time))
            
            elif(self.state == self.S4_STOPPED_AT_TOP):
                if(self.Button_2.getButtonState()==1): #same as last time, clears a button press that doesnt mean anything
                    self.Button_2.setButtonState(0)
                if(self.Button_1.getButtonState()==1):
                    self.transitionTo(self.S1_MOVING_DOWN)
                    self.Motor.MoveDOWN()
                    
                print(str(self.runs) + ': State 4 ' + str(self.curr_time-self.start_time))
                
            elif(self.state == self.S5_DO_NOTHING):
                
                print(str(self.runs) + ': State 5 ' + str(self.curr_time-self.start_time))
            
            else:
                # Invalid state code (error handling)
                pass
            
            self.runs += 1
            
            # Specifying the next time the task will run
            self.next_time = self.next_time + self.interval
    
    def transitionTo(self, newState):
        '''
        @brief      Updates the variable defining the next state to run
        '''
        self.state = newState
        

class Button:
    '''
    @brief      A pushbutton class
    @details    This class represents a button that the can be pushed by the
                someone waiting at floor 1 or 2 that will request the elevator.
    '''
    
    def __init__(self, pin, floor):
        '''
        @brief      Creates a Button object
        @param pin  A pin object that the button is connected to
        '''
        
        ## The pin object used to read the Button state
        self.pin = pin
        self.floor = floor
        
        print('Button object created attached to pin '+ str(self.pin))

    
    def getButtonState(self):
        '''
        @brief      Gets the button state.
        @details    Since there is no hardware attached this method
                    returns a randomized True or False value.
        @return     A boolean representing the state of the button.
        '''
        return self.floor
    
    
    def getsensorstate(self):
        '''
        @brief      Returns sensor state
        @details    Since there is no hardware attached this method
                    returns a randomized True or False value.
        @return     A boolean representing the state of the button.
        '''
        #adapted this one from line 169 of example code for randomization.
        return choice([0,1])
    
    
    def setButtonState(self, floor):
        '''
        @brief      Allows me to set and clear the Button states.
        @details    Since there is no hardware attached this method
                    returns a randomized True or False value.
        @return     A boolean representing the state of the button.
        '''
        self.floor = floor
        
    
class MotorDriver:
    '''
    @brief      A motor driver.
    @details    This class represents a motor driver used to move the elevator 
    up and down.
    '''
    def __init__(self):
        '''
        @brief Creates a MotorDriver Object
        '''
        pass
    
    def GoUP(self):
        '''
        @brief Moves the motor up
        '''
        print('Elevator moving UP')
    
    def GoDOWN(self):
        '''
        @brief allows elevator to go down
        '''
        print('Elevator moving DOWN')
    
    def STOP(self):
        '''
        @brief stops the elevator movement
        '''
        print('Elevator STOPPED')



























