'''
@file elevatormain.py
@author Jackson McFaul
@date Ocotober 7, 2020
'''

from FSM_elevator import TaskElevator, MotorDriver, Button

# Creating objects to pass into task constructor
Button_1 = Button('PB6', 1)
Button_2 = Button('PB7', 1)
Motor = MotorDriver()
sens_1 = 0
sens_2 = 1
first = Button('PB8', sens_1)
second = Button('PB9', sens_2)

# Creating a task object using the button and motor objects above
task1 = TaskElevator(1, Button_1, Button_2, Motor, first, second, sens_2)

# Run the tasks in sequence over and over again
for N in range(10000000): # effectively while(True):
    task1.run()
  
