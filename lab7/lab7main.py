'''
@file lab7_main.py
@brief This file runs on the nucleo and is responsible for controlling the interaction of all my files. Because I wasn't able to get the UI to work,
I instead just used data here that I could view in putty. Then, I used https://convert.town/comma-separated-list-to-column to translate that list
into something I could paste into excel to evaluate my Kp tuning. This file is adapted from lab 6 to now include reference tracking with a given CSV profile.
I had to modify the CSV profile heavily in order to get everything to work--this is discussed in my portfolio. The respective reference and response curves can 
be viewd on my portfolio as well, or by downloading the given excel file.'
This code has been adapted from lab 6 to track a reference motor profile for lab 7. 
@author Jackson McFaul
@date November 26th 2020
'''


import pyb 
from pyb import UART
import utime
from controllertask_lab7 import controller
from closedloop import ClosedLoop
from lab6_encoder import encoder
from lab7_motordriver import MotorDriver

#communication with board 
uart1 = pyb.UART(2)

#test values 
Kp = .20
#omega_ref = 1387.5
taskinterval = 200

#initialize timer for encoder 
tim = pyb.Timer(4)
tim.init(prescaler = 0, period = 0xFFFF)
tim.channel(1,pin=pyb.Pin.cpu.B6,mode=pyb.Timer.ENC_AB)
tim.channel(2,pin=pyb.Pin.cpu.B7,mode=pyb.Timer.ENC_AB)

#initialize pins for motor control--I had to switch my motor 1 wires to the motor 2 screw terminals after I encountered more hardware issues.
#During one of my runs, motor 1 just stopped all of a sudden with no error thrown, so after some trial and error I ended up moving over to M2+ and M2-
#using pins B0 and B1
pinA15 = pyb.Pin(pyb.Pin.cpu.A15,pyb.Pin.OUT_PP)
pinB0 = pyb.Pin(pyb.Pin.cpu.B0)
pinB1 = pyb.Pin (pyb.Pin.cpu.B1)
tim3 = pyb.Timer(3, freq = 20000)        

#create encoder and motor objects and enable the motor
enc = encoder(tim, 0xFFFF, taskinterval)        
motor = MotorDriver(pinA15, pinB0, pinB1, tim3)
motor.enable()

#moved the closed loop and controller functions from lab6main inside the while loop below in order to receive the correct omega reference values 
#CL = ClosedLoop(Kp, omega_ref)
#control = controller(Kp, omega_ref, motor, enc, CL)

#create empty lists to be populated with respective values 
timelist = []
speedlist = []
positionlist = []
omega_reflist = []
position_reflist = []
J_list = []

#code from Charlie's piazza post on how to interpret a CSV file on the nucleo. I edited the given file by removing the time data, and then 
#filtering it every 100 ms. I originally went for every 35ms, but because I am doing everything on the nucleo I was limited by RAM. 
ref = open('ref3.csv')
while True:
    line = ref.readline()
    if line == '':
        break
    else:
        (v,x) = line.strip().split(',');
        omega_reflist.append(float(v))
        position_reflist.append(float(x))
ref.close()
    
#create timestamps to control encoder updates and list filling         
start_time = utime.ticks_ms()
next_time = utime.ticks_add(start_time, taskinterval)

#counter for indexing the correct value of omega_ref
n = 0

#build data lists
#wait 15 seconds to break out of loop
while((next_time-start_time)<15000):
    time_current = utime.ticks_ms()
    if utime.ticks_diff(time_current, next_time)>0:
        if n>140:
            omega_ref = 0.0 
            #position_ref = 27000.0 #ref2 limit
            position_ref = 40500.0 #ref3 limit
        else:
            omega_ref = omega_reflist[n]
            position_ref = position_reflist[n]
        
        CL = ClosedLoop(Kp, omega_ref)                          #run closed loop controller with given Kp and omega ref 
        control = controller(Kp, omega_ref, motor, enc, CL)
        
        motorspeed = control.update_RPM()
        motorang = control.update_deg()
        
        time_delta = time_current - start_time
        timelist.append(time_delta)
        
        speedlist.append(motorspeed)
        positionlist.append(motorang)
        
        J_k = (((omega_ref - motorspeed)**2) + ((position_ref - motorang)**2))
        J_list.append(J_k)     
        
        n = n + 2
        next_time = utime.ticks_add(next_time, taskinterval)

#disable motor once data collection is done--if the motor doesn't disable then I run motor.disable() myself to stop the motor
#if the code has an error before it gets to this line        
motor.disable() 

#compute J by summing the entire list of error values and dividing by the length of that list
#equation given in lab 7 handout

total = 0 
K = len(J_list)
for idx in range(0, len(J_list)):
    total = total + J_list[idx] 
    J = total/(K)
       
#printing lists here so that I can input into excel and observe my tuning 
print('Time data shown below')
print(timelist)
print('----------------------------------------------------------')
print('RPM data shown below')
print(speedlist)
print('----------------------------------------------------------')
print('Position data shown below')
print(positionlist)
print('----------------------------------------------------------')
print('Performance data shown below')
print('Kp = ' + str(Kp))
print('J = ' + str(J))
print('----------------------------------------------------------')
print('Motor Disabled for hardware safety')

 
     
        
