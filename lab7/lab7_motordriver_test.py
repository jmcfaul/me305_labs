"""
@file lab6_motordriver.py
@brief This file contains the driver responsible for driving a DC motor mounted to a baseplate. This driver utilizes PWM to control
the speed at which the motor spins, as well as dictates forward and reverse drive. 
@date 11/14/20
@author: Jackson McFaul
"""

import pyb
class MotorDriver:
    '''This class implements a motor driver for the ME305 board.'''
    
    def __init__ (self, nSleep_pin, IN1_pin, IN2_pin, timer):
        ''' Creates a motor driver by initializing GPIO
        pins and turning the motor off for safety.
        @param nSLEEP_pin A pyb.Pin object to use as the enable pin.
        @param IN1_pin A pyb.Pin object to use as the input to half bridge 1.
        @param IN2_pin A pyb.Pin object to use as the input to half bridge 2.
        @param timer A pyb.Timer object to use for PWM generation on
        IN1_pin and IN2_pin. '''
        
        self.nSleep_pin = nSleep_pin
        self.IN1_pin = IN1_pin
        self.IN2_pin = IN2_pin
        self.timer = timer
        self.t3ch1 = timer.channel(3,pyb.Timer.PWM,pin=self.IN1_pin)
        self.t3ch2 = timer.channel(4,pyb.Timer.PWM,pin=self.IN2_pin)
        # self.t3ch1 = timer.channel(3,pyb.Timer.PWM,pin=self.IN1_pin)
        # self.t3ch2 = timer.channel(4,pyb.Timer.PWM,pin=self.IN2_pin)
        
        self.nSleep_pin.low()
        print('Creating a motor driver')
        
    def enable (self):
        print ('Enabling Motor')
        self.nSleep_pin.high()
        
    def disable (self):
        print ('Disabling Motor')
        self.nSleep_pin.low()
        
    def set_duty (self, duty):
        ''' This method sets the duty cycle to be sent
        to the motor to the given level. Positive values
        cause effort in one direction, negative values
        in the opposite direction.
        @param duty A signed integer holding the duty
        cycle of the PWM signal sent to the motor '''
            
        if duty > 0:
            #self.set_pulse_forward(self.duty)
            self.t3ch1.pulse_width_percent(duty)
            self.t3ch2.pulse_width_percent(0)
            
            
        elif duty < 0:
            #self.set_pulse_reverse(self.duty)
            self.t3ch2.pulse_width_percent(-1*duty)
            self.t3ch1.pulse_width_percent(0)
            
            
        self.duty = duty
              
if __name__ == '__main__':
   
    # Create the pin objects used for interfacing with the motor driver
    pin_nSLEEP = pyb.Pin (pyb.Pin.cpu.A15, pyb.Pin.OUT_PP);
    pin_IN1 = pyb.Pin (pyb.Pin.cpu.B0);
    pin_IN2 = pyb.Pin (pyb.Pin.cpu.B1)

    # Create the timer object used for PWM generation
    tim = pyb.Timer(3, freq=20000)

    # Create a motor object passing in the pins and timer
    moe = MotorDriver(pin_nSLEEP, pin_IN1, pin_IN2, tim)

    # Enable the motor driver
    moe.enable()

    # Set the duty cycle to 10 percent
    

