"""
@file lab7_test
@brief 
@date 11/14/20
@author: Jackson McFaul
"""

import pyb 
from pyb import UART
import utime
#import csv

taskinterval = 100
n = 0
timelist = []
speedlist = []
positionlist = []

omega_reflist = []
position_reflist = []

ref = open('ref2.csv')
while True:
    line = ref.readline()
    if line == '':
        break
    else:
        (v,x) = line.strip().split(',');
        omega_reflist.append(float(v))
        position_reflist.append(float(x))
ref.close()
    
        
start_time = utime.ticks_ms()
next_time = utime.ticks_add(start_time, taskinterval)


#build data lists
#wait for 1.5 seconds to acquire data
while((next_time-start_time)<15000):
    time_current = utime.ticks_ms()
    if utime.ticks_diff(time_current, next_time)>0:
        if n>140:
            omega_ref = 0 
            position_ref = 27000
        else:
            omega_ref = omega_reflist[n]
            position_ref = position_reflist[n]
        
        print(n, omega_ref, position_ref)
        #motorspeed = control.update_RPM()
        #motorang = control.update_deg()
        time_delta = time_current - start_time
        #timelist.append(time_delta)
        #speedlist.append(motorspeed)
        #positionlist.append(motorang)
        n = n + 2
        next_time = utime.ticks_add(next_time, taskinterval)
        
        
# print('Time data shown below')
# print(time_ref)
# print('----------------------------------------------------------')
print('RPM ref data shown below')
print(omega_reflist)
print('----------------------------------------------------------')
# print('Position data shown below')
# print(position_ref)
# print('----------------------------------------------------------')