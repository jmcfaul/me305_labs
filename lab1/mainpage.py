## @file mainpage.py
#  Brief doc for mainpage.py
#
#  Detailed doc for mainpage.py 
#
#  @mainpage
#
#  @section sec_intro Introduction
#  This website hosts the products of my ME-305 labs.
#
#  @section sec_lab01 Lab01
#  Function for retrieving the Fibonacci number at a selected index.
#  Accomplished through the operation of one function introduced in source code.
#  Each part of the function is described through its construction.
#  Please see https://bitbucket.org/jmcfaul/me305_labs/src/master/lab1/lab01.py for source code.
#
#  @section sec_Lecture Lecture HW1
#  Finite State Machine for operation of an elevator between 2 floors.
#  My code still has a weird attribute error that I can't figure out at the 
#  moment--will update when it is solved.
#  Please see https://bitbucket.org/jmcfaul/me305_labs/src/master/2.%20Lecture%20Homework/ for source code
#  as well as the FSM diagram.
#  
#  @author Jackson McFaul
#
#  @copyright Cal Poly Mechanical Engineering
#
#  @date Updated October 8, 2020
#