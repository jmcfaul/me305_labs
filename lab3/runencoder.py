# -*- coding: utf-8 -*-
'''
@file runencoder.py
@brief This file serves as a task to control the running of "update" from within Encoder.oy
@author Jackson McFaul
@date Ocotober 20, 2020
'''
#import pyb
#import shared_variables
import utime

class runencoder:
    '''
    Parameters
    ----------
    @param 
    @param 
    @param 
    @param
    @param 
    ----------
    '''
    S0_init =       1
    S1_getdata =    2
    S2_checkinput = 3
    S3_getvalue =   4
    S4_pass =       5
    
    def __init__(self, encoder, elapse_1):
        '''
        @brief creates the runencoder object
        
        Parameters
        ----------
       @param encoder Makes an encoder object to be updated
       @param elapse_1 Elapsed time between runs
       @param time_start_1 Timestamp for start time
       @param time_adjust_1 Timsetamp for next run
        ----------
        '''
        
        self.encoder = encoder
        self.elapse_1 = elapse_1
        self.time_start_1 = utime.ticks_ms()
        self.time_adjust_1 = utime.ticks_add(self.time_start_1, self.elapse_1)
        self.state = self.S0_init
        self.uart = uart
        
    def run(self):
        self.time_current = utime.ticks_ms()
        
        if utime.ticks_diff(self.time_current, self.time_adjust_1)>0:
            self.positionlist = []
            self.timelist = []
            self.transitionto(self.S1_getdata)
            
            self.encoder.update()
            self.time_adjust_1 = utime.ticks_add(self.time_adjust_1, self.elapse_1)
        
    def transitionto(self, newstate)        
