# -*- coding: utf-8 -*-
'''
@file lab3main.py
@brief This file controls operation of Encoder.py, renencoder.py, and userinterface.py. 
@author Jackson McFaul
@date Ocotober 20, 2020
'''
import pyb
from Encoder import ENCODER
from runencoder import runencoder
from userinterface import TaskUser

#Interaction with the board
period = 0xFFFF
tim = pyb.Timer(3)
tim.init(prescaler = 0, period = 0xFFFF)
tim.channel(1, pin=pyb.Pin.cpu.A6, mode=pyb.Timer.ENC_AB)
tim.channel(2, pin=pyb.Pin.cpu.A7, mode=pyb.Timer.ENC_AB)

# Delcaring objects and setting up tasks
encoder = ENCODER(tim, period)
time_ms = 100
task1 = runencoder(encoder, time_ms)
task2 = TaskUser(time_ms)

#Run on an infinite loop
while(True):
    task1.run()
    task2.run()
    
