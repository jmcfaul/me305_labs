# -*- coding: utf-8 -*-
'''
@file Encoder.py
@brief This file stores the ENCODER class. This class is controlled by runencoder.py. When ENCDOER updates, 
its position is calculated based on delta. Because overflow and underflow exist, correction protocol has 
been included. Delta is corrected when its magnitude is >= 1/2*period. 
@author Jackson McFaul
@date Ocotober 20, 2020
'''

import shared_variables
#import pyb
import utime 

class ENCODER:
    '''
    @brief      The encoder class encapsulates the operation of a timer.
    @details    This class will set up all the parameters necessary for this lab, as well as house 
    numerous methods required for the operation of our counter. 
    
    Parameters
        ----------
       @param S0_INIT Integer representing intialization state of update().
       @param S1_WAIT_FOR_CMD Integer representing state 1 of update () where we check for user input.
       @param position stores the position of encoder.
       @param reset An integer that brings position to 0.
        ----------
        '''

     ## Initialization state
    S0_INIT             = 0
    
    ## Wait for command state
    S1_WAIT_FOR_CMD     = 1
    
    position = 0 
    reset = 0
    
    def __init__(self, tim, period):
        '''
        @brief creates the ENCODER object
        
        Parameters
        ----------
       @param tim Creates a timer object connected to each channel.
       @param period Simply restates period for use in calculations here.
        ----------
        '''
        # self.pinA6 = pyb.pin(pyb.pin.cpu.A6)
        # self.pinA7 = pyb.pin(pyb.pin.cpu.A7)
        # self.tim3 = pyb.Timer(3, prescaler=0, period=0xFFFF)
        # self.t3ch1 = self.tim3.channel1 (1, pin=pyb.Pin.cpu.A6, mode=pyb.Timer.ENC_AB)
        # self.t3ch2 = self.tim3.channel1 (2, pin=pyb.Pin.cpu.A7, mode=pyb.Timer.ENC_AB)
        
        self.tim = tim
        
        #self.time_real_1 = utime.ticks_ms()
       
        #self.elapse_1 = elapse_1
    
        #self.time_adjust_1 = utime.ticks_add(self.time_real_1, self.elapse_1)
        
        self.period = 0XFFFF
        
        self.state = self.S0_INIT
        
    def update(self):
        #self.time_real_1 = utime.ticks_ms()
        #if utime.ticks_diff(self.time_real_1, self.time_adjust_1)>0:
        
        counter_current = self.tim.counter()
        delta = self.get_delta(self.position + self.reset, counter_current)
        
        #if(self.state == self.S0_INIT):
            
            #if utime.ticks_diff(self.time_real_1, self.time_adjust_1)>0:
        if(abs(delta)>=(self.period/2)):
            if(delta>0):
                delta = delta - self.period
            else:
                delta = delta + self.period
                            
            self.transitionTo(self.S1_WAIT_FOR_CMD)
                        
        #elif(self.state == self.S1_WAIT_FOR_CMD):
        if (shared_variables.cmd==1):
            self.reset = self.reset + self.get_position()
                #self.reset+=self.get_position()
            self.set_position(0)
            shared_variables.cmd = 0
                        
        new_position = self.position + delta
        self.set_position(new_position)
                        
        shared_variables.delta = delta
        shared_variables.position = new_position
                
            
           # self.time_adjust_1 = utime.ticks_add(self.time_adjust_1, self.elapse_1)
        
        
    def get_position(self):
        '''
        @brief Gets encoder position
        '''
        return self.position
        
        
    def set_position(self, new_position):
        '''
        @brief Sets encoder position
        '''
        self.position = new_position
        
        
    def get_delta(self, position, counter_current):
        '''
        @brief Returns a difference of 2 values
        '''
        
        delta = counter_current - position
        return delta
    
    def transitionTo(self, newState):
        '''
        @brief Updates the state variable
        '''
        self.state = newState
        
    