# -*- coding: utf-8 -*-
'''
@file shared_variables.py
@brief Stores shared variables between Encoder.py, runencoder.py, and userinterface.py
@author Jackson McFaul
@date Ocotober 20, 2020
        
Parameters
----------
@param position Integer value of position given from Encoder.py for display in userinterface.py
@param delta Integer value of delta given from Encoder.py for display in userinterface.py
@param cmd Tells Encoder.py when to reset the position to O.
----------
'''

position = None

delta = None 

cmd = 0 



