# -*- coding: utf-8 -*-
'''
@file lab2main.py
@brief This file contains the governing variables that will run the lab2FSM file. 
There are two inputs for the respective tasks in the FSM.
@author Jackson McFaul
@date Ocotober 16, 2020
'''


from lab2FSM import TaskLEDcontrol

elapse1=10000
elapse2=10000

LEDoperation = TaskLEDcontrol(elapse1, elapse2)

while True:
    LEDoperation.printLED()
    LEDoperation.pulseLED()
    