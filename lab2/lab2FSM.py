# -*- coding: utf-8 -*-
'''
@file lab2FSM.py
@brief The code file is constructed to perform two tasks: 
1. printing ON and OFF for a fixed time interval and 2. adjusting brightness of an LED on a nucleo board so that it 
follows a specific wave form. 
@author Jackson McFaul
@date Ocotober 16, 2020
'''

import pyb
# pyb allows interface with the physical pins on the nucleo
import utime
# allows for nucleo to count milliseconds

class TaskLEDcontrol:
    '''
    @brief This FSM will control multiple tasks regarding the manipulation of 
    both a virtual and physical LED.
    '''
    ## Constant defining State 1
    S1_OFF           = 1    
    
    ## Constant defining State 2
    S2_ON            = 2
    
    def __init__(self, elapse_1, elapse_2):
        '''
        @brief creates the TaskLEDcontrol object
        
        Parameters
        ----------
       @param elapse_1 : The time between each execution of task: printLED
       @param elapse_2 : The time between each execution of task: pulseLED
       @param LEDstate : The parameter which tells python whether the LED is ON or OFF
       @param LEDpulse : Stores the value of the LED brightness from 0-100
       @param LEDpulseincrease: Stores the value of the increase in brightness per iteration of pulseLED
       @param time_adjust_1: Increments the time for printLED
       @param time_adjust_2: Increments the time for pulseLED
        ----------
        '''
        #Sets the time for the first run of printLED and pulseLED
        self.time_real_1 = utime.ticks_ms()
        self.time_real_2 = utime.ticks_ms()
        
        #Parameter that takes input from lab2main for the interval to delay each iteration of printLED and pulseLED
        self.elapse_1 = elapse_1
        self.elapse_2 = elapse_2
        
        #Creats the time at which the next iteration of each task will run
        self.time_adjust_1 = utime.ticks_add(self.time_real_1, self.elapse_1)
        self.time_adjust_2 = utime.ticks_add(self.time_real_2, self.elapse_2)
        
        #Parameters delcaring the states of each task, as well as the increase in pulse
        self.LEDstate = 1
        self.LEDpulse = 0
        self.LEDpulseincrease = 10
        
        #pyb settings for interaction with the nucleo
        self.pinA5 = pyb.Pin (pyb.Pin.cpu.A5)
        self.tim2 = pyb.Timer (2, freq = 2000)
        self.t2ch1 = self.tim2.channel (1, pyb.Timer.PWM, pin=self.pinA5)
        
    def LEDstatetransition(self, S_new):
        '''
        @brief changes the state in printLED after ON or OFF is printed
        '''
        self.LEDstate=S_new
        
    def printLED(self):
        self.time_real_1 = utime.ticks_ms()
        if utime.ticks_diff(self.time_real_1, self.time_adjust_1)>=0:
        #runs task if real time value is past the adjusted time value
            if(self.LEDstate==self.S1_OFF):
                print('OFF')
                self.LEDstatetransition(self.S2_ON)
            elif(self.LEDstate==self.S2_ON):
                print('ON')
                self.LEDstatetransition(self.S1_OFF)
                
            self.time_adjust_1 = utime.ticks_add(self.time_adjust_1, self.elapse_1)
            #creates new time value by adding the designated elapsed time read from main file
            
    def pulseLED(self):
        self.time_real_2 = utime.ticks_ms()
        if utime.ticks_diff(self.time_real_2, self.time_adjust_2)>=0:
            if(self.LEDpulse>=100):
                self.LEDpulse=0
            else:
                self.LEDpulse = self.LEDpulse+self.LEDpulseincrease
                
            self.t2ch1.pulse_width_percent(self.LEDpulse)
            #adjusts brightness on physical nucleo board
            
            self.time_adjust_2 = utime.ticks_add(self.elapse_1, self.time_adjust_2)
            
            
        
        
        
